<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<!DOCTYPE HTML>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
        <title>User management</title>
    </head>
    <body>
       <form action="delete" method="post">
	       <select name="delete">
	          <c:forEach items="${users}" var="item">
	              <option>${item}</option>
	          </c:forEach>
	       </select>
	       <input type="submit" value="Delete user"/>
       </form>       
    </body>
</html>