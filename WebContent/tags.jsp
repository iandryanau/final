<%@ taglib prefix="utils" uri="/WEB-INF/tld/utils.tld" %>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<!DOCTYPE HTML>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
        <title>Tags</title>
    </head>
    <body>
        <utils:listmapping>
            ${url} - ${servlets} <br/>
        </utils:listmapping>
        <br/>
        <utils:resolveurl url="/async">
            ${url} - ${resource}
        </utils:resolveurl>
    </body>
</html>