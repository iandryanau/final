<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<!DOCTYPE HTML>
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
		<title>Users</title>
	</head>
	<body>
		Users:
		<ul>
			<c:forEach items="${users}" var="user">
			  	<li>${user}</li>
			</c:forEach>
		</ul>
	</body>
</html>