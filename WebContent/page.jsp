<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<!DOCTYPE HTML>
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
		<title>Page</title>
	</head>
	<body>
		<a href="async" style="float: right">Calculate</a>
		<form method="post" action="user">
			First Name: <input name="firstName" value="${user.firstName}"><br>
			Last Name: <input name="lastName" value="${user.lastName}"><br>
			Short info: <textarea name="info">${user.shortInfo}</textarea><br>
			<input type="submit" value="Save"/>
		</form>
		<form method="post" action="logout">
			<input type="submit" value="Log out"/>
		</form>		
	</body>
</html>