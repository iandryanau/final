package by.epamtraining.utils;

public class LogCreator {
	public static String createForwardMsg(String from, String to) {
		final StringBuilder sb = new StringBuilder();
		sb.append("Forwarding from ").append(from).append(" to ").append(to);
		return sb.toString();
	}
}
