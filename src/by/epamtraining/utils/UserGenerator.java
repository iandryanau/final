package by.epamtraining.utils;

import java.util.ArrayList;
import java.util.List;

import by.epamtraining.beans.User;

public class UserGenerator {
	private final static String[] FIRST_NAMES = { "Imya1", "Imya2", "Imya3", "Imya4", "Imya5", "Imya6", "Imya7", "Imya8",
			"Imya9", "Imya10" };
	private final static String[] LAST_NAMES = { "Familiya1", "Familiya2", "Familiya3", "Familiya4", "Familiya5", "Familiya6",
			"Familiya7", "Familiya8", "Familiya9", "Familiya10", };
	private final static String[] PASSWORDS = { "pw1", "pw2", "pw3", "pw4", "pw5", "pw6",
			"pw7", "pw8", "pw9", "pw10" };
	private final static int LEN = 10;

	public static List<User> generate(int amount) {
		final List<User> users = new ArrayList<User>(amount);
		
		for (int i = 0; i < amount; i++) {
			final String firstname = FIRST_NAMES[(int) (Math.random() * LEN)];
			final String lastname = LAST_NAMES[(int) (Math.random() * LEN)];
			final StringBuilder sb = new StringBuilder();
			sb.append(firstname.charAt(0)).append(lastname.charAt(0));
			final String login = sb.toString();
			final String password = PASSWORDS[(int) (Math.random() * LEN)];
			users.add(new User(firstname, lastname, login, password, "I am a generated user"));
		}

		return users;
	}
}
