package by.epamtraining.wrappers;

import javax.servlet.RequestDispatcher;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletRequestWrapper;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import by.epamtraining.utils.LogCreator;

public class LogServletRequestWrapper extends HttpServletRequestWrapper {
	private final Logger LOGGER = LogManager.getLogger();

	public LogServletRequestWrapper(HttpServletRequest request) {
		super(request);
	}

	@Override
	public RequestDispatcher getRequestDispatcher(String path) {
		RequestDispatcher dispatcher = super.getRequestDispatcher(path);
		LOGGER.info(LogCreator.createForwardMsg(getRequestURL().toString(), path));
		return dispatcher;
	}

}
