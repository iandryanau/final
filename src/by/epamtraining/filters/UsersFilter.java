package by.epamtraining.filters;

import java.io.IOException;
import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;

import by.epamtraining.Constants;

@WebFilter(filterName = "UsersFilter")
public class UsersFilter extends AbstractHttpFilter {

	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
			throws IOException, ServletException {
		HttpServletRequest httpReq = (HttpServletRequest) request;
		if (httpReq.getMethod().equalsIgnoreCase("get")
				&& httpReq.getSession().getAttribute(Constants.USER_ATTR) == null) {
			request.getRequestDispatcher(Constants.LOGIN_HTML).forward(request, response);
		} else {
			chain.doFilter(request, response);
		}
	}

}
