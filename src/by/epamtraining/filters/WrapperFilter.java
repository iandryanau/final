package by.epamtraining.filters;

import java.io.IOException;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;

import by.epamtraining.wrappers.LogServletRequestWrapper;

@WebFilter(filterName = "WrapperFilter", asyncSupported = true)
public class WrapperFilter extends AbstractHttpFilter {

	@Override
	public void doFilter(ServletRequest req, ServletResponse res, FilterChain chain)
			throws IOException, ServletException {
		chain.doFilter(new LogServletRequestWrapper((HttpServletRequest) req), res);
	}

}
