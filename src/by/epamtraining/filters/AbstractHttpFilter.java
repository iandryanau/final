package by.epamtraining.filters;

import javax.servlet.Filter;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;

public abstract class AbstractHttpFilter implements Filter {
	protected FilterConfig config;

	public void init(FilterConfig fConfig) throws ServletException {
		this.config = fConfig;
	}
	
}
