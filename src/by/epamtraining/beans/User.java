package by.epamtraining.beans;

import javax.servlet.http.HttpSessionActivationListener;
import javax.servlet.http.HttpSessionBindingEvent;
import javax.servlet.http.HttpSessionBindingListener;
import javax.servlet.http.HttpSessionEvent;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import by.epamtraining.database.DBHelper;
import by.epamtraining.exceptions.DatabaseException;

public class User implements HttpSessionActivationListener, HttpSessionBindingListener {
	private final Logger LOGGER = LogManager.getLogger();

	private String firstName;
	private String lastName;
	private String login;
	private String password;
	private String shortInfo;

	public User(String firstName, String lastName, String login, String password, String shortInfo) {
		super();
		this.firstName = firstName;
		this.lastName = lastName;
		this.login = login;
		this.password = password;
		this.shortInfo = shortInfo;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getLogin() {
		return login;
	}

	public void setLogin(String login) {
		this.login = login;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getShortInfo() {
		return shortInfo;
	}

	public void setShortInfo(String shortInfo) {
		this.shortInfo = shortInfo;
	}

	@Override
	public String toString() {
		final StringBuilder sb = new StringBuilder();
		sb.append(firstName).append(" ").append(lastName).append(" (").append(login).append(")");
		return sb.toString();
	}

	@Override
	public void sessionDidActivate(HttpSessionEvent se) {
		LOGGER.info(String.format("Session activated: user = %s", login));
	}

	@Override
	public void sessionWillPassivate(HttpSessionEvent se) {
		LOGGER.info(String.format("Session will passivate: user = %s", login));
	}

	@Override
	public void valueBound(HttpSessionBindingEvent event) {
		final String LOG_IN = "Log In";
		LOGGER.info(String.format("Value bound: user = %s", toString()));
		try {
			new DBHelper().addLog(login, LOG_IN);
		} catch (DatabaseException e) {
			LOGGER.error(e.getMessage(), e);
		}
	}

	@Override
	public void valueUnbound(HttpSessionBindingEvent event) {
		final String LOG_OUT = "Log Out";
		LOGGER.info(String.format("Value unbound: user = %s", toString()));
		try {
			new DBHelper().addLog(login, LOG_OUT);
		} catch (DatabaseException e) {
			LOGGER.error(e.getMessage(), e);
		}
	}

}
