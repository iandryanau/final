package by.epamtraining.listeners;

import javax.servlet.ServletContextAttributeEvent;
import javax.servlet.ServletContextAttributeListener;
import javax.servlet.annotation.WebListener;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

@WebListener
public class ApplicationAttributeListener implements ServletContextAttributeListener {
	private final Logger LOGGER = LogManager.getLogger(); 

	public void attributeAdded(ServletContextAttributeEvent scae) {
		final StringBuilder sb = new StringBuilder();
		sb.append("Attribute added: ").append(scae.getName()).append(" = ").append(scae.getValue());
		LOGGER.info(sb.toString());
	}

	public void attributeRemoved(ServletContextAttributeEvent scae) {
		final StringBuilder sb = new StringBuilder();
		sb.append("Attribute removed: ").append(scae.getName()).append(" = ").append(scae.getValue());
		LOGGER.info(sb.toString());
	}

	public void attributeReplaced(ServletContextAttributeEvent scae) {
		final String name = scae.getName();
		final StringBuilder sb = new StringBuilder();
		sb.append("Attribute replaced: ").append(name).append(" = ").append(scae.getValue()).append(" with ")
				.append(scae.getServletContext().getAttribute(name));
		LOGGER.info(sb.toString());
	}
	
}
