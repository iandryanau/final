package by.epamtraining.listeners;

import java.util.Iterator;
import java.util.List;

import javax.naming.InitialContext;
import javax.servlet.ServletContext;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.annotation.WebListener;

import org.apache.catalina.UserDatabase;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import by.epamtraining.Constants;
import by.epamtraining.beans.User;
import by.epamtraining.database.DBHelper;
import by.epamtraining.exceptions.DatabaseException;

@WebListener
public class ApplicationListener implements ServletContextListener {
	private final Logger LOGGER = LogManager.getLogger();

	public void contextDestroyed(ServletContextEvent sce) {
	    try {
            ((UserDatabase) sce.getServletContext().getAttribute(Constants.USER_DB_ATTR)).close();
        } catch (Exception e) {
            LOGGER.error(e);
        }
		LOGGER.info("Context destroyed");
	}

	public void contextInitialized(ServletContextEvent sce) {
		final ServletContext context = sce.getServletContext();
		final StringBuilder sb = new StringBuilder();
		sb.append("Server info: ").append(context.getServerInfo());
		LOGGER.info(sb.toString());
		sb.setLength(0);

		sb.append("Servlet context name: ").append(context.getServletContextName());
		LOGGER.info(sb.toString());
		sb.setLength(0);

		sb.append("Available servlets: ");
		final Iterator<String> iterator = context.getServletRegistrations().keySet().iterator();
		while (iterator.hasNext()) {
			sb.append(iterator.next()).append(", ");
		}
		LOGGER.info(sb.deleteCharAt(sb.length() - 2).toString());

		List<User> users = null;
		try {
			users = new DBHelper().selectUsers();
		} catch (DatabaseException e) {
			LOGGER.error(e);
		}
		context.setAttribute(Constants.USERS_ATTR, users);
		UserDatabase userDatabase = null;
        try {
            userDatabase = (UserDatabase) new InitialContext().lookup("java:comp/env/UserDatabase");
            userDatabase.open();
        } catch (Exception e) {
            LOGGER.error(e);
        }
		context.setAttribute(Constants.USER_DB_ATTR, userDatabase);
	}

}
