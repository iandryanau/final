package by.epamtraining.listeners;

import javax.servlet.ServletRequestAttributeEvent;
import javax.servlet.ServletRequestAttributeListener;
import javax.servlet.annotation.WebListener;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

@WebListener
public class RequestAttributeListener implements ServletRequestAttributeListener {
	private final Logger LOGGER = LogManager.getLogger();
	
    public void attributeRemoved(ServletRequestAttributeEvent srae)  { 
    	final StringBuilder sb = new StringBuilder();
		sb.append("Attribute removed: ").append(srae.getName()).append(" = ").append(srae.getValue());
		LOGGER.info(sb.toString());
    }

    public void attributeAdded(ServletRequestAttributeEvent srae)  { 
    	final StringBuilder sb = new StringBuilder();
		sb.append("Attribute added: ").append(srae.getName()).append(" = ").append(srae.getValue());
		LOGGER.info(sb.toString());
    }

    public void attributeReplaced(ServletRequestAttributeEvent srae)  { 
    	final String name = srae.getName();
		final StringBuilder sb = new StringBuilder();
		sb.append("Attribute replaced: ").append(name).append(" = ").append(srae.getValue()).append(" with ")
				.append(srae.getServletRequest().getAttribute(name));
		LOGGER.info(sb.toString());
    }
	
}
