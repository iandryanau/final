package by.epamtraining.listeners;

import javax.servlet.annotation.WebListener;
import javax.servlet.http.HttpSessionEvent;
import javax.servlet.http.HttpSessionListener;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

@WebListener
public class SessionListener implements HttpSessionListener {
	private final Logger LOGGER = LogManager.getLogger();
	
    public void sessionCreated(HttpSessionEvent se)  { 
         LOGGER.info("Session has been created");
    }

    public void sessionDestroyed(HttpSessionEvent se)  { 
    	 LOGGER.info("Session has been destroyed");
    }
	
}
