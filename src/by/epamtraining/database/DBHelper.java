package by.epamtraining.database;

import java.io.IOException;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import by.epamtraining.beans.User;
import by.epamtraining.exceptions.DatabaseException;

public class DBHelper {
	private final static String ADD_USERS = "INSERT INTO `users`(`firstName`, `lastName`, `login`, `password`, `info`) VALUES (?, ?, ?, ?, ?)";
	private final static String SELECT_USERS = "SELECT * FROM `users`";
	private final static String SELECT_USER = "SELECT `firstName`, `lastName`, `password`, `info` FROM `users` WHERE `login` = ?";
	private final static String UPDATE_USER = "UPDATE `users` SET `firstName` = ?, `lastName` = ?, `info` = ? WHERE `login` = ?";
	// private final static String DELETE_USERS = "DELETE FROM `users`";

	private final static String ADD_LOG = "INSERT INTO `log`(`login`, `datetime`, `action`) VALUES (?, NOW(), ?)";

	private final static String DRIVER;
	private final static String URL;
	private final static String USER;
	private final static String PASSWORD;

	private final static Logger LOGGER = LogManager.getLogger();

	static {
		InputStream input = null;

		final ClassLoader classLoader = Thread.currentThread().getContextClassLoader();
		input = classLoader.getResourceAsStream("database.properties");

		final Properties prop = new Properties();
		try {
			prop.load(input);
		} catch (IOException e) {
			LOGGER.error(e);
		}

		DRIVER = prop.getProperty("driver");
		URL = prop.getProperty("url");
		USER = prop.getProperty("user");
		PASSWORD = prop.getProperty("password");

		if (input != null) {
			try {
				input.close();
			} catch (IOException e) {
				LOGGER.warn(e);
			}
		}

		try {
			Class.forName(DRIVER);
		} catch (ClassNotFoundException e) {
			LOGGER.error(e);
		}
	}

	/*
	 * public DBHelper() throws IOException { InputStream input = null; try {
	 * final ClassLoader classLoader =
	 * Thread.currentThread().getContextClassLoader(); input =
	 * classLoader.getResourceAsStream("database.properties");
	 * 
	 * final Properties prop = new Properties(); prop.load(input);
	 * 
	 * DRIVER = prop.getProperty("driver"); URL = prop.getProperty("url"); USER
	 * = prop.getProperty("user"); PASSWORD = prop.getProperty("password");
	 * 
	 * try { Class.forName(DRIVER); } catch (ClassNotFoundException e) {
	 * LOGGER.error(e); } } finally { if (input != null) { input.close(); } } }
	 */

	public List<User> selectUsers() throws DatabaseException {
		final int FIRST_NAME_INDEX = 2;
		final int LAST_NAME_INDEX = 3;
		final int LOGIN_INDEX = 4;
		final int PASSWORD_INDEX = 5;
		final int INFO_INDEX = 6;

		final String SELECTION_ERROR = "Users selection failed";

		List<User> users = new ArrayList<>();

		Connection con = null;
		Statement st = null;
		ResultSet rs = null;
		try {
			con = DriverManager.getConnection(URL, USER, PASSWORD);
			st = con.createStatement();
			rs = st.executeQuery(SELECT_USERS);
			while (rs.next()) {
				final String first = rs.getString(FIRST_NAME_INDEX);
				final String last = rs.getString(LAST_NAME_INDEX);
				final String login = rs.getString(LOGIN_INDEX);
				final String password = rs.getString(PASSWORD_INDEX);
				final String info = rs.getString(INFO_INDEX);
				final User user = new User(first, last, login, password, info);
				users.add(user);
			}
			return users;
		} catch (SQLException e) {
			throw new DatabaseException(SELECTION_ERROR, e);
		} finally {
			closeResultSet(rs);
			closeStatement(st);
			closeConnection(con);
		}
	}

	public User selectUser(String login) throws DatabaseException {
		final int LOGIN_INDEX = 1;

		final int FIRST_NAME_INDEX = 1;
		final int LAST_NAME_INDEX = 2;
		final int PASSWORD_INDEX = 3;
		final int INFO_INDEX = 4;

		final String SELECTION_ERROR = "User selection failed";

		Connection con = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			con = DriverManager.getConnection(URL, USER, PASSWORD);
			ps = con.prepareStatement(SELECT_USER);
			ps.setString(LOGIN_INDEX, login);
			rs = ps.executeQuery();
			if (rs.next()) {
				final String first = rs.getString(FIRST_NAME_INDEX);
				final String last = rs.getString(LAST_NAME_INDEX);
				final String password = rs.getString(PASSWORD_INDEX);
				final String info = rs.getString(INFO_INDEX);
				return new User(first, last, login, password, info);
			} else {
				return null; // is it fine?
			}
		} catch (SQLException e) {
			throw new DatabaseException(SELECTION_ERROR, e);
		} finally {
			closeResultSet(rs);
			closeStatement(ps);
			closeConnection(con);
		}
	}

	public void addUsers(List<User> users) throws DatabaseException {
		final String INSERTION_ERROR = "User insertion failed";
		final String ROLLBACK_ERROR = "Rollback failed";

		Connection con = null;
		PreparedStatement ps = null;
		try {
			con = DriverManager.getConnection(URL, USER, PASSWORD);
			con.setAutoCommit(false);
			// deleteUsers(con);
			ps = con.prepareStatement(ADD_USERS);
			for (User user : users) {
				ps.setString(1, user.getFirstName());
				ps.setString(2, user.getLastName());
				ps.setString(3, user.getLogin());
				ps.setString(4, user.getPassword());
				ps.setString(5, user.getShortInfo());
				ps.addBatch();
			}
			ps.executeBatch();
			con.commit();
		} catch (SQLException e) {
			try {
				con.rollback();
			} catch (SQLException sqlex) {
				LOGGER.error(ROLLBACK_ERROR, sqlex);
			}
			throw new DatabaseException(INSERTION_ERROR, e);

		} finally {
			closeStatement(ps);
			closeConnection(con);
		}
	}

	public void addUser(User user) throws DatabaseException {
		List<User> list = new ArrayList<User>();
		list.add(user);
		addUsers(list);
	}

	public void updateUser(String firstName, String lastName, String info, String login) throws DatabaseException {
		final String UPDATE_ERROR = "User update failed";

		final int FIRST_NAME_INDEX = 1;
		final int LAST_NAME_INDEX = 2;
		final int INFO_INDEX = 3;
		final int LOGIN_INDEX = 4;

		Connection con = null;
		PreparedStatement ps = null;
		try {
			con = DriverManager.getConnection(URL, USER, PASSWORD);
			ps = con.prepareStatement(UPDATE_USER);
			ps.setString(FIRST_NAME_INDEX, firstName);
			ps.setString(LAST_NAME_INDEX, lastName);
			ps.setString(INFO_INDEX, info);
			ps.setString(LOGIN_INDEX, login);
			ps.execute();
		} catch (SQLException e) {
			throw new DatabaseException(UPDATE_ERROR, e);
		} finally {
			closeStatement(ps);
			closeConnection(con);
		}
	}

	public void addLog(String login, String action) throws DatabaseException {
		final String INSERTION_ERROR = "Log insertion failed";

		final int LOGIN_INDEX = 1;
		final int ACTION_INDEX = 2;

		Connection con = null;
		PreparedStatement ps = null;
		try {
			con = DriverManager.getConnection(URL, USER, PASSWORD);
			ps = con.prepareStatement(ADD_LOG);
			ps.setString(LOGIN_INDEX, login);
			ps.setString(ACTION_INDEX, action);
			ps.execute();
		} catch (SQLException e) {
			throw new DatabaseException(INSERTION_ERROR, e);
		} finally {
			closeStatement(ps);
			closeConnection(con);
		}
	}

	private void closeConnection(Connection con) {
		if (con != null) {
			try {
				con.close();
			} catch (SQLException e) {
				LOGGER.warn("Could not close connection", e);
			}
		}
	}

	private void closeStatement(Statement stmt) {
		if (stmt != null) {
			try {
				stmt.close();
			} catch (SQLException e) {
				LOGGER.warn("Could not close statement", e);
			}
		}
	}

	private void closeResultSet(ResultSet rs) {
		if (rs != null) {
			try {
				rs.close();
			} catch (SQLException e) {
				LOGGER.warn("Could not close result set", e);
			}
		}
	}

	/*
	 * private void deleteUsers(Connection con) throws SQLException { Statement
	 * st = null; try { st = con.createStatement(); st.execute(DELETE_USERS); }
	 * finally { if (st != null) { st.close(); } } }
	 */

}
