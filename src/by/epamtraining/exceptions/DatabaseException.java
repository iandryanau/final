package by.epamtraining.exceptions;

public class DatabaseException extends Exception {
	private static final long serialVersionUID = 1L;

	public DatabaseException(String msg, Throwable ex) {
		super(msg, ex);
	}
	
}
