package by.epamtraining.tags;

import java.io.IOException;
import java.util.Iterator;
import java.util.Map;

import javax.servlet.ServletContext;
import javax.servlet.ServletRegistration;
import javax.servlet.jsp.JspContext;
import javax.servlet.jsp.JspException;
import javax.servlet.jsp.PageContext;
import javax.servlet.jsp.tagext.SimpleTagSupport;

import by.epamtraining.Constants;

public class ResolveTag extends SimpleTagSupport {
    private String url;

    public void setUrl(String url) {
        this.url = url;
    }

    @Override
    public void doTag() throws JspException, IOException {        
        final JspContext jspContext = getJspContext();        
        if (jspContext instanceof PageContext) {
            final PageContext pageContext = (PageContext) jspContext;
            final String servletName = findServletName(pageContext.getServletContext());
            pageContext.setAttribute(Constants.URL_ATTR, url);
            pageContext.setAttribute(Constants.RESOURCE_ATTR, servletName);
            getJspBody().invoke(null);
        } else {
            // it isn't even servlet related
            throw new JspException(Constants.NOT_SERVLET_API_ERROR);
        }
    }

    private String findServletName(ServletContext context) {
        final Iterator<? extends Map.Entry<String, ? extends ServletRegistration>> iterator = context
                .getServletRegistrations().entrySet().iterator();
        while (iterator.hasNext()) {
            final Map.Entry<String, ? extends ServletRegistration> entry = iterator.next();
            for (String mapping : entry.getValue().getMappings()) {
                if (mapping.equals(url)) {
                    return entry.getKey();
                }
            }
        }
        return null;
    }

}
