package by.epamtraining.tags;

import java.util.Iterator;
import java.util.Map;

import javax.servlet.ServletRegistration;
import javax.servlet.jsp.JspException;
import javax.servlet.jsp.tagext.TagSupport;

import by.epamtraining.Constants;

public class ListMappingTag extends TagSupport {
    private static final long serialVersionUID = 1L;
    private Iterator<? extends Map.Entry<String, ? extends ServletRegistration>> iterator;

    @Override
    public int doStartTag() throws JspException {
        iterator = pageContext.getServletContext().getServletRegistrations().entrySet().iterator();
        return evaluate(EVAL_BODY_INCLUDE);
    }
    
    @Override
    public int doAfterBody() throws JspException {
        return evaluate(EVAL_BODY_AGAIN);
    }

    private int evaluate(int returnEvalValue) {
        if (iterator.hasNext()) {
            final Map.Entry<String, ? extends ServletRegistration> entry = iterator.next();
            pageContext.setAttribute(Constants.URL_ATTR, entry.getKey());
            pageContext.setAttribute(Constants.SERVLETS_ATTR, String.join(", ", entry.getValue().getMappings()));
            return returnEvalValue;
        }
        return SKIP_BODY;        
    }

}
