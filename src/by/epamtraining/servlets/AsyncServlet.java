package by.epamtraining.servlets;

import java.io.IOException;

import javax.servlet.AsyncContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import by.epamtraining.Constants;

@WebServlet(asyncSupported = true, urlPatterns = { "/async" })
public class AsyncServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private final Logger LOGGER = LogManager.getLogger();

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		final AsyncContext acontext = request.startAsync();
		acontext.start(new Runnable() {
			
			@Override
			public void run() {		
				final int FIRST_VALUE = 4000;
				try {
					Thread.sleep(FIRST_VALUE);
				} catch (InterruptedException e) {
					LOGGER.warn(Constants.SLEEP_ERROR, e);
				}
				request.setAttribute("firstValue", FIRST_VALUE);
				acontext.dispatch("/asyncdispatch");				
			}
			
		});
				
	}

}
