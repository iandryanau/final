package by.epamtraining.servlets;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.HttpConstraint;
import javax.servlet.annotation.ServletSecurity;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.catalina.User;
import org.apache.catalina.UserDatabase;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import by.epamtraining.Constants;

@WebServlet("/delete")
@ServletSecurity(@HttpConstraint(rolesAllowed = "user_management"))
public class DeleteServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;
    private final Logger LOGGER = LogManager.getLogger();

    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        final List<String> users = new ArrayList<String>();
        final Iterator<User> iterator = ((UserDatabase) getServletContext().getAttribute(Constants.USER_DB_ATTR))
                .getUsers();
        while (iterator.hasNext()) {
            users.add(iterator.next().getUsername());
        }
        request.setAttribute(Constants.USERS_ATTR, users);
        request.getRequestDispatcher(Constants.DELETE_JSP).forward(request, response);
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String userToDelete = request.getParameter("delete");
        UserDatabase userDatabase = (UserDatabase) getServletContext().getAttribute(Constants.USER_DB_ATTR);
        userDatabase.removeUser(userDatabase.findUser(userToDelete));
        try {
            userDatabase.save();           
        } catch (Exception e) {
            LOGGER.warn(e);
        }
       
        doGet(request, response);
    }

}
