package by.epamtraining.servlets;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import by.epamtraining.Constants;
import by.epamtraining.beans.User;
import by.epamtraining.database.DBHelper;
import by.epamtraining.exceptions.DatabaseException;

@WebServlet("/user")
public class LoginServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private final Logger LOGGER = LogManager.getLogger();

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		Object user = request.getSession().getAttribute(Constants.USER_ATTR);
		request.setAttribute(Constants.USER_ATTR, user);
		request.getRequestDispatcher(Constants.PAGE_JSP).forward(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		final HttpSession session = request.getSession();
		
		final String firstName = request.getParameter(Constants.FIRST_PARAM);
		final String lastName = request.getParameter(Constants.LAST_PARAM);
		final String info = request.getParameter(Constants.INFO_PARAM);
		
		if (firstName != null && lastName != null && info != null) {
			User user = (User) session.getAttribute(Constants.USER_ATTR);
			try {
				new DBHelper().updateUser(firstName, lastName, info, user.getLogin());
				user.setFirstName(firstName);
				user.setLastName(lastName);
				user.setShortInfo(info);
				response.sendRedirect("user");
			} catch (DatabaseException e) {
				LOGGER.error(e);
			}			
			return;
		}
		
		final String login = request.getParameter(Constants.LOGIN_PARAM);
		final String password = request.getParameter(Constants.PASSWORD_PARAM);

		User user = null;
		try {
			user = new DBHelper().selectUser(login);
			if (user != null && user.getPassword().equals(password)) {
				request.setAttribute(Constants.USER_ATTR, user);
				session.setAttribute(Constants.USER_ATTR, user);
				request.getRequestDispatcher(Constants.PAGE_JSP).forward(request, response);
			} else {
				LOGGER.warn(Constants.INCORRECT_INPUT_MSG);
				request.setAttribute(Constants.MSG_ATTR, Constants.INCORRECT_INPUT_MSG);
				request.getRequestDispatcher(Constants.ERROR_JSP).forward(request, response);
			}
		} catch (DatabaseException e) {
			LOGGER.error(e);
		}
	}

}
