package by.epamtraining.servlets;

import java.io.IOException;

import javax.servlet.AsyncContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import by.epamtraining.Constants;

@WebServlet(asyncSupported = true, urlPatterns = { "/asyncdispatch" })
public class AsyncDispatchServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private final Logger LOGGER = LogManager.getLogger();

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		final AsyncContext acontext = request.isAsyncStarted() ? request.getAsyncContext() : request.startAsync();
		acontext.start(new Runnable() {

			@Override
			public void run() {
				final int SECOND_VALUE = 3000;

				try {
					Thread.sleep(SECOND_VALUE);
				} catch (InterruptedException e) {
					LOGGER.warn(Constants.SLEEP_ERROR, e);
				}
				acontext.getRequest().setAttribute("secondValue", SECOND_VALUE);
				acontext.dispatch("/async.jsp");
			}

		});
	}

}
