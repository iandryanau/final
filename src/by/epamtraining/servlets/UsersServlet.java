package by.epamtraining.servlets;

import java.io.IOException;
import java.util.concurrent.atomic.AtomicBoolean;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.UnavailableException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import by.epamtraining.Constants;
import by.epamtraining.beans.User;
import by.epamtraining.database.DBHelper;
import by.epamtraining.exceptions.DatabaseException;

@WebServlet("/users")
public class UsersServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private final AtomicBoolean available = new AtomicBoolean();
	private final Logger LOGGER = LogManager.getLogger();

	@Override
	public void init() throws ServletException {
		available.set(!Boolean.parseBoolean(getServletContext().getInitParameter(Constants.UNAVAILABLE_PARAM)));
	}

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		final ServletContext context = getServletContext();
		if (!available.get()) {
			available.set(true);
			throw new UnavailableException("Something's wrong",
					Integer.parseInt(context.getInitParameter(Constants.UNAVAILABLE_TIME_PARAM)));
		}
		request.setAttribute(Constants.USERS_ATTR, context.getAttribute(Constants.USERS_ATTR));
		request.getRequestDispatcher(Constants.USERS_JSP).forward(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// assuming data is valid
		final String first = request.getParameter(Constants.FIRST_PARAM);
		final String last = request.getParameter(Constants.LAST_PARAM);
		final String login = request.getParameter(Constants.LOGIN_PARAM);
		final String password = request.getParameter(Constants.PASSWORD_PARAM);
		final String info = request.getParameter(Constants.INFO_PARAM);

		final DBHelper helper = new DBHelper();
		try {
			if (helper.selectUser(login) == null) {
				helper.addUser(new User(first, last, login, password, info));
			} else {
				LOGGER.warn(Constants.LOGIN_EXISTS_MSG);
				request.setAttribute(Constants.MSG_ATTR, Constants.LOGIN_EXISTS_MSG);
				request.getRequestDispatcher(Constants.ERROR_JSP).forward(request, response);
			}
		} catch (DatabaseException e) {
			LOGGER.error(e);
		}
	}

}
