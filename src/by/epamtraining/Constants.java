package by.epamtraining;

public class Constants {
	public final static String USERS_ATTR = "users";
	public final static String USER_DB_ATTR = "userDB";
	public final static String USERS_AMOUNT_PARAM = "usersAmount";
	public final static String UNAVAILABLE_PARAM = "unavailable";
	public final static String UNAVAILABLE_TIME_PARAM = "unavailableTime";
	
	public final static String FIRST_PARAM = "firstName";
	public final static String LAST_PARAM = "lastName";
	public final static String LOGIN_PARAM = "login";
	public final static String PASSWORD_PARAM = "password";
	public final static String INFO_PARAM = "info";
	
	public final static String MSG_ATTR = "msg";
	public final static String INCORRECT_INPUT_MSG = "Incorrect input";
	public final static String LOGIN_EXISTS_MSG = "Login already exists";
	
	public final static String USER_ATTR = "user";
	
	public final static String USERS_JSP = "users.jsp";
	public final static String ERROR_JSP = "error.jsp";
	public final static String PAGE_JSP = "page.jsp";
	public final static String LOGIN_HTML = "login.html";
	public final static String DELETE_JSP = "WEB-INF/delete.jsp";
	
	public final static String SLEEP_ERROR = "Sleep threw an exception";
	
	public final static String URL_ATTR = "url";
	public final static String RESOURCE_ATTR = "resource";
	public final static String SERVLETS_ATTR = "servlets";
	public final static String NOT_SERVLET_API_ERROR = "Not using Servlet API";
}
